#include "file.h"
#include "bmp.h"
#include "rotate.h"
#include <stdlib.h>

static const char* NOT_ENOUGH = "Not enough args";
static const char* CANT_OPEN_READ = "Can't open file for reading";
static const char* CANT_OPEN_WRITE = "Can't open file for writing";
static const char* CANT_READ = "Can't read the input file";
static const char* CANT_CLOSED_IN = "Can't close the input file";
static const char* CANT_CLOSED_OUT = "Can't close the output file";
static const char* CANT_WRITE = "Can't write to the output file";

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "%s", NOT_ENOUGH);
        return -1;
    }
    
    char* const inputf = argv[1];
    char* const outputf= argv[2];

    FILE *input = NULL;
    FILE *output= NULL;

    struct image *img = image_create(0, 0);
    
    if(!open_file(&input, inputf, "r")) {
        printf("%s", CANT_OPEN_READ);
        return -1;
    }

    enum read_status status = from_bmp(input, img);

    if (status !=READ_OK) {
        printf("%s", CANT_READ);
        image_destroy(img);
        if (close_file(&input)) {
            printf("%s", CANT_CLOSED_IN);
            return -1;
        }
        return -1;
    }

    if(!(open_file(&output, outputf, "w"))) {
        image_destroy(img);
        if (close_file(&input)) {
            printf("%s", CANT_CLOSED_IN);
            return -1;
        }
        printf("%s", CANT_OPEN_WRITE);
        return -1;
    }

    struct image* rotated = rotate(img);

    enum write_status status1 = to_bmp(output, rotated);

    if (status1!= WRITE_OK) {
        printf("%s", CANT_WRITE);
        image_destroy(img);
        image_destroy(rotated);
        return 0;
    }

    if(!close_file(&input)) {
        printf("%s", CANT_CLOSED_IN);
        image_destroy(img);
        image_destroy(rotated);
        return -1;
    }

    if(!close_file(&output)) {
        printf("%s", CANT_CLOSED_OUT);
        image_destroy(img);
        image_destroy(rotated);
        return -1;
    }

    image_destroy(img);
    image_destroy(rotated);

    return 0;
}
