#include "rotate.h"

struct image *rotate(struct image* img) {
    struct image* img1 = image_create(img->height, img->width);
    img1->pixels = malloc(sizeof(struct pixel) * img1->height * img1->width);
    for (size_t i = 0; i < img1->height; i++) {
        for (size_t j = 0; j < img1->width; j++) {
            struct pixel pixel = img->pixels[(img1->width - j - 1) * img->width + i];
            img1->pixels[i * img1->width + j] = pixel;
        }
    }
    return img1;
}


